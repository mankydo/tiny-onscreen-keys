	
CXX=g++
CFLAGS=-g -lpthread -ldl -Wall -Wextra -Werror -no-pie 
INCLUDE=-I
SRCDIR=sources/
OBJDIR=obj/
SOURCES=$(wildcard $(SRCDIR)*.cpp)
DEPS = 

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	$(CXX) -c $< -o $@ `pkg-config gtkmm-3.0 --cflags`
	@echo "\n sources are: " $(SOURCES) 
	@echo "\n get arg: "$@" and "$<""
	@echo "   Object created:" $@
	@echo "\n"

gtktest: $(SOURCES:$(SRCDIR)%.cpp=$(OBJDIR)%.o)
	$(CXX) -o TinyOnscreenKeys $^ `pkg-config gtkmm-3.0 --libs` $(CFLAGS)
	@echo "\nCompiled the program successfuly!"



