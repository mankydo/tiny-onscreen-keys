#include <iostream>
#include <string>
#include <gtkmm/label.h>
#include "TinyOnscreenWindow.h"

HelloWorld::HelloWorld()
{

  set_border_width(10);

  add(m_vbox);

  m_vbox.pack_start(m_button(), Gtk::PackOptions::PACK_SHRINK); 

  show_all_children();

  set_accept_focus(false);
  set_keep_above(true);

}

HelloWorld::~HelloWorld()
{
}

void HelloWorld::on_button_clicked()
{
  std::cout << "Hello World" << std::endl;
}