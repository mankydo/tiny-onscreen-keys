#ifndef ONSCREENBUTTON_H
#define ONSCREENBUTTON_H

using namespace std;
#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/widget.h>
#include <gtkmm/box.h>
#include <gtkmm/window.h>
#include <gtkmm/eventbox.h>

class OnScreenButton
{
public:
  OnScreenButton();
  Gtk::Widget& operator()();
  ~OnScreenButton();

  Gtk::Box    box {Gtk::ORIENTATION_HORIZONTAL};
  Gtk::Button m_button;

protected:
  //Signal handlers:
  void on_button_clicked();

private:
  Gtk::EventBox m_eventBox;
  Gtk::Label  m_label {" :test label button: "};

};

#endif