#include <iostream>
#include "OnScreenButton.h"

OnScreenButton::OnScreenButton()
{
    m_button.set_label("test button");
    m_button.set_margin_left(100);  

    box.add(m_eventBox);

    m_button.signal_clicked().connect(
        [=](){
            std::cout << "Hello World" << std::endl;
        }
    );

    box.pack_start(m_button);

    m_eventBox.add(m_label);  
    box.pack_start(m_eventBox);

    m_eventBox.signal_button_press_event().connect(
        [=](gpointer data){
            std::cout << ":::button label pressed\n" << std::endl;
            return true;
        }
    );
    m_eventBox.signal_button_release_event().connect(
        [=](gpointer data){
            std::cout << "::::button label released\n" << std::endl;
            return true;
        }
    );
} 

Gtk::Widget& OnScreenButton::operator()() {
  return box;
}

OnScreenButton::~OnScreenButton()
{}

