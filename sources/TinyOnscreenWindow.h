#ifndef TINYONSCREENWINDOW_H
#define TINYONSCREENWINDOW_H

#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/eventbox.h>
#include "OnScreenButton.h"

class HelloWorld : public Gtk::Window
{

public:
  HelloWorld();
  ~HelloWorld() override;

protected:
  //Signal handlers:
  void on_button_clicked();

private:
    Gtk::Box    m_vbox      {Gtk::ORIENTATION_VERTICAL};
    OnScreenButton m_button;
};

#endif // GTKMM_EXAMPLE_HELLOWORLD_H